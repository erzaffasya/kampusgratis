<?php
use App\Http\Controllers\KontenVideoController;
use App\Http\Controllers\KontenDokumenController;
use App\Http\Controllers\KelasController;
use App\Http\Controllers\IklanController;
use App\Http\Controllers\ArtikelController;
use App\Http\Controllers\ProfilController;
use App\Http\Controllers\JobChannelController;
use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/dosen', function () {
    return view('dosen.index');
})->name('dashboard');
Route::get('/matkul', function () {
    return view('dosen.tugas.index');
})->name('dashboard');
Route::get('/showMatkul', function () {
    return view('dosen.tugas.tambahTugas');
})->name('dashboard');
Route::get('/showUjian', function () {
    return view('dosen.tugas.tambahUjian');
})->name('dashboard');
/*
Route::get('/', function () {
    return view('admin.index');
})->name('dashboard');
Route::get('/form', function () {
    return view('form');
})->name('form');
Route::get('/tab', function () {
    return view('tab');
})->name('form');
*/
// Route::get('/konten-Video', function () {
//     return view('admin.kontenVideo.index');
// })->name('konten-Video');

// Route::get('/konten-dokumen', function () {
//     return view('admin.kontenDokumen.index');
// })->name('konten-dokumen');

// Route::get('/kelas', function () {
//     return view('admin.kelas.index');
// })->name('kelas');
Route::get('/', [AuthController::class, 'showFormLogin'])->name('login');
Route::get('login', [AuthController::class, 'showFormLogin'])->name('login');
Route::post('login', [AuthController::class, 'login']);
Route::get('register', [AuthController::class, 'showFormRegister'])->name('register');
Route::post('register', [AuthController::class, 'register']);

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', function () {
        return view('admin.index');
    })->name('dashboard');
    Route::get('/form', function () {
        return view('form');
    })->name('form');
    Route::get('/tab', function () {
        return view('tab');
    })->name('form');
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');
 
});

Route::resource('kontenVideo', KontenVideoController::class);
Route::resource('kontenDokumen', KontenDokumenController::class);
Route::resource('kelas', KelasController::class);
Route::resource('artikel', ArtikelController::class);
Route::resource('iklan', IklanController::class);
Route::resource('profil', ProfilController::class);
Route::resource('jobChannel', JobChannelController::class);
Route::get('/linkstorage', function () {
    Artisan::call('storage:link');
    return '<h1>Storage Linked</h1>';
});
