<x-app-layout> 
    {{-- BLABLA --}}
    <div class="col-md-6 ">
        {{-- <div class="col-xl-12"> --}}
        <div class="card">
          <div class="card-header d-flex pb-0 p-3">
            <h6 class="my-auto">Buat Ujian</h6>
            <div class="nav-wrapper position-relative ms-auto w-50">
              <ul class="nav nav-pills nav-fill p-1" role="tablist">
                <li class="nav-item">
                  <a class="nav-link mb-0 px-0 py-1 active" data-bs-toggle="tab" href="#cam1" role="tab" aria-controls="cam1" aria-selected="true">
                    Pilihan Ganda
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link mb-0 px-0 py-1" data-bs-toggle="tab" href="#cam2" role="tab" aria-controls="cam2" aria-selected="false">
                    Isian
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="card-body p-3 mt-2">
            <div class="tab-content" id="v-pills-tabContent">
              {{-- <div class="" style="background-image: url('../../assets/img/bg-smart-home-1.jpg'); background-size:cover;"> --}}
              <div class="card-body tab-pane fade show position-relative active  border-radius-lg" id="cam1" role="tabpanel" aria-labelledby="cam1">
                <form role="form text-left" action="{{route('kontenVideo.store')}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <div class="mb-3">
                    <label for="exampleFormControlSelect1">Jenis</label>
                    <select class="form-control" name="kategori" id="exampleFormControlSelect1" required>
                      <option value="Basic">UTS</option>
                      <option value="Intermediate">UAS</option>
                    </select>
                  </div>
                  <div class="mb-3">
                    <label for="exampleFormControlSelect1">Pengantar</label>
                    <textarea class="form-control" aria-label="With textarea" name="deskripsi" rows="4" required></textarea>
                  </div>
                  <div class="mb-3">
                    <label for="exampleFormControlSelect1">File Soal</label>
                    <input type="file" class="form-control" name="file" required>
                  </div>
                  <div class="mb-3">
                    <label for="exampleFormControlSelect1">Status</label>
                    <select class="form-control" name="kategori" id="exampleFormControlSelect1" required>
                      <option value="Basic">Active</option>
                      <option value="Intermediate">Pending</option>
                    </select>
                  </div>
                  <div class="text-center">
                    <button type="submit" class="btn bg-gradient-dark w-100 my-4 mb-2">Tambah</button>
                  </div>
                </form>
              </div>
  
              <div class="card-body tab-pane fade position-relative  border-radius-lg" id="cam2" role="tabpanel" aria-labelledby="cam2">
                <form role="form text-left" action="{{route('kontenDokumen.store')}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <div class="mb-3">
                    <label for="exampleFormControlSelect1">BAB</label>
                    <input type="text" class="form-control" name="judul" placeholder="Masukkan Judul" required>
                  </div>
                  <div class="mb-3">
                    <label for="exampleFormControlSelect1">Pengantar</label>
                    <textarea class="form-control" aria-label="With textarea" name="deskripsi" rows="4" required></textarea>
                  </div>
                  <div class="mb-3">
                    <label for="exampleFormControlSelect1">Kategori</label>
                    <select class="form-control" name="kategori" id="exampleFormControlSelect1" required>
                      <option value="Basic">Active</option>
                      <option value="Intermediate">Peding</option>
                    </select>
                  </div>
                  <div class="mb-3">
                    <label for="exampleFormControlSelect1">File Pendukung</label>
                    <input type="file" class="form-control" name="file" required>
                  </div>
                  <div class="text-center">
                    <button type="submit" class="btn bg-gradient-dark w-100 my-4 mb-2">Tambah</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    {{-- @push('scripts')
    <script>
      tinymce.init({
        selector: 'textarea',
        plugins: 'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        toolbar_mode: 'floating',
     });
    </script>
    @endpush --}}
  </x-app-layout>